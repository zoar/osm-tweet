#!/usr/bin/env ruby
#coding: UTF-8

require 'twitter'
require 'json'
require 'open-uri'
require 'time'
require 'rexml/document'

between = 1
between_sec = between * 86400

now = Time.now
yesterday = now - between_sec

changeset = 0
node = 0
way = 0

current_path = File.expand_path(File.dirname(__FILE__))
setting_file = current_path + '/setting.json'

def get_changeset(id)
   url = "https://www.openstreetmap.org/api/0.6/changeset/#{id}/download"
   changeset_xml = open(url).read
   node = changeset_xml.scan(/<node /).size
   way = changeset_xml.scan(/<way /).size
   
   size = [node , way]
end

unless File.exist?(setting_file)
   mkconfig = current_path + '/make-config.rb'
   system mkconfig
end

json_data = open(setting_file) do |config|
   JSON.load(config)
end

user = json_data['user']

url = "https://www.openstreetmap.org/api/0.6/changesets/?display_name=#{json_data['user']}"

history_xml = open(url).read

history_doc = REXML::Document.new(history_xml)

history_doc.elements.each('osm/changeset') do |element|
   closed_at = element.attributes['closed_at']
   if Time.parse(closed_at).between?(yesterday, now)
      changeset += 1
      id = element.attributes['id']
      size = get_changeset(id)
      node += size[0]
      way += size[1]
   end
end

unless changeset == 0
   tweet_text = "#{user}が24時間以内に行った OpenStreetMap の編集は変更セット数 #{changeset} 、ノード数 #{node} 、ウェイ数 #{way} です。"
   
   client = Twitter::REST::Client.new do |config|
      config.consumer_key = json_data['ck']
      config.consumer_secret = json_data['cs']
      config.access_token = json_data['at']
      config.access_token_secret = json_data['as']
   end

   client.update(tweet_text)
end

exit 0
