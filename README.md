#osm-tweet

スクリプト実行時から24時間以内に行われた [OpenStreetMap](http://www.openstreetmap.org) の編集数をツイートします。

`make-config.rb` を実行すると Twitter の認証と、 OpenStreetMap のユーザーネーム(ディスプレイネーム)を入力して設定ファイルを作成します。

ディレクトリに `setting.json` が存在しなければ `osm-tweet.rb` から `make-config.rb` を呼び出すようにはしています。

cron に登録しても実行できるんじゃないかと思います。

----
Copyright (c) 2015 Zoar  
[Released under the MIT LICENCE](http://opensource.org/licenses/mit-license.php)