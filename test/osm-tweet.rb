#!/usr/bin/env ruby
#coding: UTF-8

require 'time'
require 'open-uri'
require 'json'
require 'rexml/document'

between = 1
between_sec = between * 86400

baseurl = 'http://www.openstreetmap.org/api/0.6/'

now = Time.now
yesterday = now - between_sec

current_path = Dir::getwd
setting_file = current_path + '/../setting.json'

json_data = open(setting_file) do |config|
   JSON.load(config)
end

#history_xml = open("#{baseurl}changesets/?display_name=#{json_data['user']}").read

changeset_xml = open('./download.xml','r:UTF-8').read

#changeset_doc = REXML::Documen.new(chageset_xml)

node = 0
way = 0

node = changeset_xml.scan(/<node /).size
way  = changeset_xml.scan(/<way /).size
p node
p way

#p changeset_doc[]


exit 0

history_xml = open('./sample.xml').read

osm_xml = REXML::Document.new(history_xml)

osm_xml.elements.each('osm/changeset') do |element|
   closed_at = element.attributes['closed_at']
   if Time.parse(closed_at).between?(yesterday, now)
      puts element.attributes['id']
   end
end