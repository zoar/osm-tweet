#!/usr/bin/env ruby
# coding: UTF-8

require 'time'


p Time.parse('2015/07/12 01:27:05').getlocal
p Time.now
p Time.now - 86400
p 24 * 60 * 60
tgt = Time.parse('2015/07/12 01:27:05').getlocal

p tgt.between?(Time.now - 86400, Time.now)